# Colorful Shippable [![Run Status](https://api.shippable.com/projects/56963f631895ca44746824d9/badge?branch=master)](https://app.shippable.com/projects/56963f631895ca44746824d9)

Test repository to check color output in Shippable build logs.

The build executes two scripts, one for the basic 16 colors supported by pretty much every terminal and one for the advanced 256 color palette.
